<?php
header("Access-Control-Allow-Origin: *"); 

switch ($_SERVER['SERVER_NAME']) {
  case 'inventory.tanguy.ovh':
    // production database
    $dbhost = "tanguyovsgmath.mysql.db";
    $dbuser = "tanguyovsgmath";
    $dbpass = "Hdtu7swwk";
    $dbbase = "tanguyovsgmath";
    break;
  
  default:
    // developpment database
    $dbhost = "127.0.0.1:3306";
    $dbuser = "root";
    $dbpass = "root";
    $dbbase = "mydatabase";
    break;
}

// Get data from the query
$data = json_decode($_GET['data']);
$request = $_GET['request'];
$table = $_GET['table'];

$base= mysqli_connect($dbhost,  $dbuser, $dbpass, $dbbase);
if (mysqli_connect_errno()) 
  die('Could not connect: ' . mysql_error());

  mysqli_query( $base,"SET character_set_client=utf8mb4");
  mysqli_query($base,"SET character_set_connection=utf8mb4");
  mysqli_query($base, "SET character_set_results=utf8mb4");

switch ($request) {
  ////////////////////////////
  //        ALL
  ///////////////////////////
  case "allDocsFormated":
    $sql = 
      "SELECT 
        responsible._id,
        responsible.acronyme,
        office.city AS office
      FROM
        $table
      JOIN office ON `office`.`_id`=`responsible`.`office_id`";
    if ($result = mysqli_query( $base, $sql )){
        while ($row = mysqli_fetch_assoc($result)) {
        $response[] = $row;
      }
      echo  json_encode($response);
    } else {
      echo  json_encode($result);
    }

    mysqli_close($base);

      break;
  ////////////////////////////
  //        ALL
  ///////////////////////////    
  case 'allDocs':
    $sql = "SELECT * FROM $table ";
    if ($result = mysqli_query( $base, $sql )){
      while ($row = mysqli_fetch_assoc($result)) {
        $response[] = $row;
      }
    }
    mysqli_close($base);
    echo  json_encode($response);
    break;
  ////////////////////////////
  //        ADD
  ///////////////////////////
  case "addDoc":
    //first we create $values array. it contains all values that you need to insert to the db
    $values = array();
    $j=0;
    // echo json_encode($test[0]);
    for($i = 0; $i < count($data); $i++){
        $values[$j]['serialNumber'] = $data[$i]->serialNumber;
        $values[$j]['reference'] = $data[$i]->reference;
        $values[$j]['setNumber'] = $data[$i]->setNumber;
        $values[$j]['stock'] = $data[$i]->stock;
        $values[$j]['responsible'] = $data[$i]->responsible;
        $values[$j]['initialLocation'] = $data[$i]->initialLocation;
        $values[$j]['actualLocation'] = $data[$i]->actualLocation;
        $values[$j]['customer'] = $data[$i]->customer;
        $values[$j]['contact'] = $data[$i]->contact;
        $values[$j]['loanStartDate'] = $data[$i]->loanStartDate;
        $values[$j]['loanDuration'] = $data[$i]->loanDuration;
        $values[$j]['comments'] = $data[$i]->comments;
        $values[$j]['doNotInventory'] = $data[$i]->doNotInventory;
        $values[$j]['status'] = $data[$i]->status;
        $values[$j]['lastEditedBy'] = $data[$i]->lastEditedBy;
        $values[$j]['lastEditedAt'] = $data[$i]->lastEditedAt;
        $values[$j]['createdBy'] = $data[$i]->createdAt;
        $values[$j]['createdAt'] = $data[$i]->createdAt;
        $values[$j]['assembly'] = $data[$i]->assembly;
        $values[$j]['type'] = $data[$i]->type;
        $values[$j]['quantity'] = $data[$i]->quantity;
        //and fill others here
        //fill all cols that you wrote inside your query with the correct order
        $j++;
    }
    // echo json_encode($values);

    //now create (value1,value2,...),(value1,value2,...),...
    $query = NULL;
    foreach($values as $value){
        $tmp = NULL;
        foreach($value as $v){
            $tmp .= ($v=='')? 'NULL,' : "'$v',";
        }
        $tmp = rtrim($tmp,',');
        $query .= "($tmp),";
    }
    $query = rtrim($query,',');
    // echo $query;
        $test = mysqli_query($base,
          "INSERT INTO `material` (`serialNumber`, `reference`, `setNumber`, `stock`, `responsible`, `initialLocation`, `actualLocation`, `customer`, `contact`, `loanStartDate`, `loanDuration`, `comments`, `doNotInventory`, `status`, `lastEditedBy`, `lastEditedAt`, `createdBy`, `createdAt`, `assembly`, `type`, `quantity`) 
          VALUES $query"
        );
        echo "Inserted successfully";
      mysqli_close($base);
      break;
  ////////////////////////////
  //        UPDATE
  ///////////////////////////
  case "updateDoc":
      $values = $data;
      $sql = "SELECT * FROM $table WHERE serialNumber='".$values->serialNumber."'";
      $result = mysqli_query( $base, $sql );
      if(mysqli_num_rows($result) == 1){
        // Document exists
        mysqli_query($base,"UPDATE $table SET 
          `reference`='".$values->reference."',
          `setNumber`='".$values->setNumber."',
          `stock`='".$values->stock."',
          `responsible`='".$values->responsible."',
          `initialLocation`='".$values->initialLocation."',
          `actualLocation`='".$values->actualLocation."',
          `customer`='".$values->customer."',
          `contact`='".$values->contact."',
          `loanStartDate`='".$values->loanStartDate."',
          `loanDuration`='".$values->loanDuration."',
          `comments`='".$values->comments."',
          `doNotInventory`='".$values->doNotInventory."',
          `status`='".$values->status."',
          `lastEditedBy`='".$values->lastEditedBy."',
          `lastEditedAt`='".$values->lastEditedAt."'
          `assembly`='".$values->assembly."',
          `type`='".$values->type."',
          `quantity`='".$values->quantity."'
          WHERE serialNumber='".$values->serialNumber."'"
        );
        echo "Updated successfully";
      }else{
        echo "Serial Number does not exist.";
      }
      mysqli_close($base);
      break;

  ////////////////////////////
  //        DELETE
  ///////////////////////////
  case "deleteDoc":
      $sql = "SELECT * FROM $table WHERE _id='".$data->_id."'";
      $result = mysqli_query( $base, $sql );
      if(mysqli_num_rows($result) == 1){
        // Document exists
        mysqli_query($base,"DELETE FROM $table WHERE _id='".$data->_id."'");
        echo "Deleted successfully";
      }else{
        echo "Serial Number does not exist.";
      }
      mysqli_close($base);
      break;
  ////////////////////////////
  //        GET DOC BY ID
  ///////////////////////////
  case "docById":
    $sql = 
    "SELECT 
      *
    FROM 
      $table 
    WHERE _id='".$data->_id."'";
    if ($result = mysqli_query( $base, $sql )){
        while ($row = mysqli_fetch_assoc($result)) {
        $response = $row;
        }
        if ($result->num_rows == null) {
          echo "No match!";
        } else {
          echo  json_encode($response);;
        }
    };
    mysqli_close($base);
    break;
  default: echo "No match!";
    break;
}

?>