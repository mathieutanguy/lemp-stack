<?php
header("Access-Control-Allow-Origin: *"); 

$id = $_GET['id'];
$cookie_name = "access_token";

if(!isset($_COOKIE[$cookie_name])) {
  // echo "Cookie named '" . $cookie_name . "' is not set!";
} else {
  // echo "Cookie '" . $cookie_name . "' is set!<br>";
  // echo "Value is: " . $_COOKIE[$cookie_name];
  $token = $_COOKIE[$cookie_name];
}

// GET TOKEN

require_once 'HTTP/Request2.php';


$request = new HTTP_Request2();
$request->setUrl('https://dev-br-fr.eu.auth0.com/api/v2/users/'.$id);
$request->setMethod(HTTP_Request2::METHOD_GET);
$request->setConfig(array(
  'follow_redirects' => TRUE
));
$request->setHeader(array(
  // 'Authorization' => 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik5vdDd1QkhXcm1iemVFd1Y4LTFiVyJ9.eyJpc3MiOiJodHRwczovL2Rldi1ici1mci5ldS5hdXRoMC5jb20vIiwic3ViIjoidm9XOVFUcXI5UjZwQ05MRUNTZU4zd0cyc1lycmdWVkRAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vZGV2LWJyLWZyLmV1LmF1dGgwLmNvbS9hcGkvdjIvIiwiaWF0IjoxNTg3MDE5NjA3LCJleHAiOjE1ODcxMDYwMDcsImF6cCI6InZvVzlRVHFyOVI2cENOTEVDU2VOM3dHMnNZcnJnVlZEIiwic2NvcGUiOiJyZWFkOmNsaWVudF9ncmFudHMgY3JlYXRlOmNsaWVudF9ncmFudHMgZGVsZXRlOmNsaWVudF9ncmFudHMgdXBkYXRlOmNsaWVudF9ncmFudHMgcmVhZDp1c2VycyB1cGRhdGU6dXNlcnMgZGVsZXRlOnVzZXJzIGNyZWF0ZTp1c2VycyByZWFkOnVzZXJzX2FwcF9tZXRhZGF0YSB1cGRhdGU6dXNlcnNfYXBwX21ldGFkYXRhIGRlbGV0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgY3JlYXRlOnVzZXJzX2FwcF9tZXRhZGF0YSByZWFkOnVzZXJfY3VzdG9tX2Jsb2NrcyBjcmVhdGU6dXNlcl9jdXN0b21fYmxvY2tzIGRlbGV0ZTp1c2VyX2N1c3RvbV9ibG9ja3MgY3JlYXRlOnVzZXJfdGlja2V0cyByZWFkOmNsaWVudHMgdXBkYXRlOmNsaWVudHMgZGVsZXRlOmNsaWVudHMgY3JlYXRlOmNsaWVudHMgcmVhZDpjbGllbnRfa2V5cyB1cGRhdGU6Y2xpZW50X2tleXMgZGVsZXRlOmNsaWVudF9rZXlzIGNyZWF0ZTpjbGllbnRfa2V5cyByZWFkOmNvbm5lY3Rpb25zIHVwZGF0ZTpjb25uZWN0aW9ucyBkZWxldGU6Y29ubmVjdGlvbnMgY3JlYXRlOmNvbm5lY3Rpb25zIHJlYWQ6cmVzb3VyY2Vfc2VydmVycyB1cGRhdGU6cmVzb3VyY2Vfc2VydmVycyBkZWxldGU6cmVzb3VyY2Vfc2VydmVycyBjcmVhdGU6cmVzb3VyY2Vfc2VydmVycyByZWFkOmRldmljZV9jcmVkZW50aWFscyB1cGRhdGU6ZGV2aWNlX2NyZWRlbnRpYWxzIGRlbGV0ZTpkZXZpY2VfY3JlZGVudGlhbHMgY3JlYXRlOmRldmljZV9jcmVkZW50aWFscyByZWFkOnJ1bGVzIHVwZGF0ZTpydWxlcyBkZWxldGU6cnVsZXMgY3JlYXRlOnJ1bGVzIHJlYWQ6cnVsZXNfY29uZmlncyB1cGRhdGU6cnVsZXNfY29uZmlncyBkZWxldGU6cnVsZXNfY29uZmlncyByZWFkOmhvb2tzIHVwZGF0ZTpob29rcyBkZWxldGU6aG9va3MgY3JlYXRlOmhvb2tzIHJlYWQ6ZW1haWxfcHJvdmlkZXIgdXBkYXRlOmVtYWlsX3Byb3ZpZGVyIGRlbGV0ZTplbWFpbF9wcm92aWRlciBjcmVhdGU6ZW1haWxfcHJvdmlkZXIgYmxhY2tsaXN0OnRva2VucyByZWFkOnN0YXRzIHJlYWQ6dGVuYW50X3NldHRpbmdzIHVwZGF0ZTp0ZW5hbnRfc2V0dGluZ3MgcmVhZDpsb2dzIHJlYWQ6c2hpZWxkcyBjcmVhdGU6c2hpZWxkcyBkZWxldGU6c2hpZWxkcyByZWFkOmFub21hbHlfYmxvY2tzIGRlbGV0ZTphbm9tYWx5X2Jsb2NrcyB1cGRhdGU6dHJpZ2dlcnMgcmVhZDp0cmlnZ2VycyByZWFkOmdyYW50cyBkZWxldGU6Z3JhbnRzIHJlYWQ6Z3VhcmRpYW5fZmFjdG9ycyB1cGRhdGU6Z3VhcmRpYW5fZmFjdG9ycyByZWFkOmd1YXJkaWFuX2Vucm9sbG1lbnRzIGRlbGV0ZTpndWFyZGlhbl9lbnJvbGxtZW50cyBjcmVhdGU6Z3VhcmRpYW5fZW5yb2xsbWVudF90aWNrZXRzIHJlYWQ6dXNlcl9pZHBfdG9rZW5zIGNyZWF0ZTpwYXNzd29yZHNfY2hlY2tpbmdfam9iIGRlbGV0ZTpwYXNzd29yZHNfY2hlY2tpbmdfam9iIHJlYWQ6Y3VzdG9tX2RvbWFpbnMgZGVsZXRlOmN1c3RvbV9kb21haW5zIGNyZWF0ZTpjdXN0b21fZG9tYWlucyB1cGRhdGU6Y3VzdG9tX2RvbWFpbnMgcmVhZDplbWFpbF90ZW1wbGF0ZXMgY3JlYXRlOmVtYWlsX3RlbXBsYXRlcyB1cGRhdGU6ZW1haWxfdGVtcGxhdGVzIHJlYWQ6bWZhX3BvbGljaWVzIHVwZGF0ZTptZmFfcG9saWNpZXMgcmVhZDpyb2xlcyBjcmVhdGU6cm9sZXMgZGVsZXRlOnJvbGVzIHVwZGF0ZTpyb2xlcyByZWFkOnByb21wdHMgdXBkYXRlOnByb21wdHMgcmVhZDpicmFuZGluZyB1cGRhdGU6YnJhbmRpbmcgcmVhZDpsb2dfc3RyZWFtcyBjcmVhdGU6bG9nX3N0cmVhbXMgZGVsZXRlOmxvZ19zdHJlYW1zIHVwZGF0ZTpsb2dfc3RyZWFtcyBjcmVhdGU6c2lnbmluZ19rZXlzIHJlYWQ6c2lnbmluZ19rZXlzIHVwZGF0ZTpzaWduaW5nX2tleXMiLCJndHkiOiJjbGllbnQtY3JlZGVudGlhbHMifQ.S9bnr1FRgFINwadarCuslSIbNw0QghH7OUSnHdotxJ7AH0zu3QYWJOl4aly1T47dVD7-Kz0HjTUqw2nthAp2jhj2Z6WM6NTQ-8EyLnc7RPa8GIoFq7lN4wtpct9rYkpuDEX-OeLwVC6lcRqHuAvc_7KdwEMhNFSSmuvPVc8TjS5ougBXs9Jefxu7fKrgMgneEOX_sEgymUiRnFQPCZyO0RAUDD6Q7q2eSXf-06V9UOY2zMyMo27ecmXrSQkCfS295Zj4VwsGIqimo86prsjOgMrkoq-dWAVSnFBArNdFJgza8wAXPI2ZOvjID5dz8uneqAKVIN6tDtptGvBLgSkIOQ',
  'Authorization' => 'Bearer '.$token,
  'Cookie' => 'did=s%3Av0%3Ad8bea140-7edf-11ea-abd1-2feadacf7639.4liKpsWwot%2F729ckUyoi18F8a31ZFtmgT%2BdnbxKVM90; did_compat=s%3Av0%3Ad8bea140-7edf-11ea-abd1-2feadacf7639.4liKpsWwot%2F729ckUyoi18F8a31ZFtmgT%2BdnbxKVM90'
));
try {
  $response = $request->send();
  if ($response->getStatus() == 200) {
    echo $response->getBody();
    // echo 'Already valid token';
  }
  else {
    // echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
    // $response->getReasonPhrase();
    // GET NEW TOKEN
    $request = new HTTP_Request2();
    $request->setUrl('https://dev-br-fr.eu.auth0.com/oauth/token');
    $request->setMethod(HTTP_Request2::METHOD_POST);
    $request->setConfig(array(
      'follow_redirects' => TRUE
    ));
    $request->setHeader(array(
      'Content-Type' => 'application/x-www-form-urlencoded',
      'Cookie' => 'did=s%3Av0%3Ad8bea140-7edf-11ea-abd1-2feadacf7639.4liKpsWwot%2F729ckUyoi18F8a31ZFtmgT%2BdnbxKVM90; did_compat=s%3Av0%3Ad8bea140-7edf-11ea-abd1-2feadacf7639.4liKpsWwot%2F729ckUyoi18F8a31ZFtmgT%2BdnbxKVM90'
    ));
    $request->addPostParameter(array(
      'grant_type' => 'client_credentials',
      'client_id' => 'voW9QTqr9R6pCNLECSeN3wG2sYrrgVVD',
      'client_secret' => '_Rx8ZqLALJdW5Dn2GUZkeGFoEuU_xxxmD4kXwFVu0NmCAPhIOVn8o0s1MpBaToX8',
      'audience' => 'https://dev-br-fr.eu.auth0.com/api/v2/'
    ));
    try {
      $response = $request->send();
      if ($response->getStatus() == 200) {
        // echo 'got fresh new token';
        // echo $response->getBody();
        $token_info = json_decode($response->getBody());
        $cookie_value = $token_info->access_token;
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/", "", true); // 86400 = 1 day
        // echo ($token_info->access_token);
        // Request again
        $request = new HTTP_Request2();
        $request->setUrl('https://dev-br-fr.eu.auth0.com/api/v2/users/'.$id);
        $request->setMethod(HTTP_Request2::METHOD_GET);
        $request->setConfig(array(
          'follow_redirects' => TRUE
        ));
        $request->setHeader(array(
          'Authorization' => 'Bearer '.$token_info->access_token,
          'Cookie' => 'did=s%3Av0%3Ad8bea140-7edf-11ea-abd1-2feadacf7639.4liKpsWwot%2F729ckUyoi18F8a31ZFtmgT%2BdnbxKVM90; did_compat=s%3Av0%3Ad8bea140-7edf-11ea-abd1-2feadacf7639.4liKpsWwot%2F729ckUyoi18F8a31ZFtmgT%2BdnbxKVM90'
        ));
        try {
          $response = $request->send();
          if ($response->getStatus() == 200) {
            echo $response->getBody();
          }
          else {
            echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
            $response->getReasonPhrase();
          }
        }
        catch(HTTP_Request2_Exception $e) {
          echo 'Error: ' . $e->getMessage();
        }
      }
      else {
        echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
        $response->getReasonPhrase();
      }
    }
    catch(HTTP_Request2_Exception $e) {
      echo 'Error: ' . $e->getMessage();
    }
  }
}
catch(HTTP_Request2_Exception $e) {
  echo 'Error: ' . $e->getMessage();
}