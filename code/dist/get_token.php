<?php

header("Access-Control-Allow-Origin: *"); 

require_once 'HTTP/Request2.php';
$request = new HTTP_Request2();
$request->setUrl('https://dev-br-fr.eu.auth0.com/oauth/token');
$request->setMethod(HTTP_Request2::METHOD_POST);
$request->setConfig(array(
  'follow_redirects' => TRUE
));
$request->setHeader(array(
  'Content-Type' => 'application/x-www-form-urlencoded',
  'Cookie' => 'did=s%3Av0%3Ad8bea140-7edf-11ea-abd1-2feadacf7639.4liKpsWwot%2F729ckUyoi18F8a31ZFtmgT%2BdnbxKVM90; did_compat=s%3Av0%3Ad8bea140-7edf-11ea-abd1-2feadacf7639.4liKpsWwot%2F729ckUyoi18F8a31ZFtmgT%2BdnbxKVM90'
));
$request->addPostParameter(array(
  'grant_type' => 'client_credentials',
  'client_id' => 'voW9QTqr9R6pCNLECSeN3wG2sYrrgVVD',
  'client_secret' => '_Rx8ZqLALJdW5Dn2GUZkeGFoEuU_xxxmD4kXwFVu0NmCAPhIOVn8o0s1MpBaToX8',
  'audience' => 'https://dev-br-fr.eu.auth0.com/api/v2/'
));
try {
  $response = $request->send();
  if ($response->getStatus() == 200) {
    echo $response->getBody();
  }
  else {
    echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
    $response->getReasonPhrase();
  }
}
catch(HTTP_Request2_Exception $e) {
  echo 'Error: ' . $e->getMessage();
}