<?php
header("Access-Control-Allow-Origin: *"); 

switch ($_SERVER['SERVER_NAME']) {
  case 'inventory.tanguy.ovh':
    // production database
    $dbhost = "tanguyovsgmath.mysql.db";
    $dbuser = "tanguyovsgmath";
    $dbpass = "Hdtu7swwk";
    $dbbase = "tanguyovsgmath";
    break;
  
  default:
    // developpment database
    $dbhost = "127.0.0.1:3306";
    $dbuser = "root";
    $dbpass = "root";
    $dbbase = "mydatabase";
    break;
}

// Get data from the query
$data = json_decode($_GET['data']);
$request = $_GET['request'];
$table = $_GET['table'];

$base= mysqli_connect($dbhost,  $dbuser, $dbpass, $dbbase);
if (mysqli_connect_errno()) 
  die('Could not connect: ' . mysql_error());

  mysqli_query( $base,"SET character_set_client=utf8mb4");
  mysqli_query($base,"SET character_set_connection=utf8mb4");
  mysqli_query($base, "SET character_set_results=utf8mb4");

switch ($request) {
  ////////////////////////////
  //        ALL
  ///////////////////////////
  case "allDocsFormated":
    $sql = 
      "SELECT 
        {$table}._id,
        {$table}.name,
        office.city AS office,
        {$table}.lastEditedBy,
        {$table}.lastEditedAt,
        {$table}.createdBy,
        {$table}.createdAt
      FROM
        $table
      JOIN office ON `office`.`_id`=`{$table}`.`office_id`";
    if ($result = mysqli_query( $base, $sql )){
        while ($row = mysqli_fetch_assoc($result)) {
        $response[] = $row;
      }
      echo  json_encode($response);
    } else {
      echo  json_encode($result);
    }

    mysqli_close($base);

      break;
  ////////////////////////////
  //        ALL
  ///////////////////////////    
  case 'allDocs':
    $sql = "SELECT * FROM $table ";
    if ($result = mysqli_query( $base, $sql )){
      while ($row = mysqli_fetch_assoc($result)) {
        $response[] = $row;
      }
    }
    mysqli_close($base);
    echo  json_encode($response);
    break;
  ////////////////////////////
  //        ADD
  ///////////////////////////
  case "addDoc":
    //first we create $values array. it contains all values that you need to insert to the db
    $values = array();
    // echo json_encode($test[0]);
    $values['name'] = $data->name;
    $values['office_id'] = $data->office_id;
    $values['comment'] = $data->comment;
    $values['lastEditedBy'] = $data->lastEditedBy;
    $values['lastEditedAt'] = $data->lastEditedAt;
    $values['createdBy'] = $data->createdAt;
    $values['createdAt'] = $data->createdAt;
        //and fill others here
        //fill all cols that you wrote inside your query with the correct order

    //now create (value1,value2,...),(value1,value2,...),...
    $query = NULL;
    // foreach($values as $value){
    $tmp = NULL;
    foreach($values as $v){
        $tmp .= ($v=='')? 'NULL,' : "'$v',";
    }
    $tmp = rtrim($tmp,',');
    $query .= "($tmp),";
    // }
    $query = rtrim($query,',');
    // echo $query;
    $test = mysqli_query($base,
      "INSERT INTO $table (`name`, `office_id`, `comment`, `lastEditedBy`, `lastEditedAt`, `createdBy`, `createdAt`) 
      VALUES $query"
    );
    echo "Inserted successfully";
    mysqli_close($base);
    break;
  ////////////////////////////
  //        UPDATE
  ///////////////////////////
  case "updateDoc":
    $values = $data;

    $values = array();
    // echo json_encode($test[0]);
    $values['_id'] = $data->_id;
    $values['name'] = $data->name;
    $values['office_id'] = $data->office_id;
    $values['comment'] = ($data->comment=='')? 'NULL' : "'$data->comment'";
    // echo json_encode($values['comment']);
    $values['lastEditedBy'] = ($data->lastEditedBy=='')? 'NULL' : "'$data->lastEditedBy'";
    $values['lastEditedAt'] = ($data->lastEditedAt=='')? 'NULL' : "'$data->lastEditedAt'";

    // $tmp = NULL;
    // foreach($values as $v){
    //   $v .= ($v=='')? NULL : $v;
    // }
    $sql = "SELECT * FROM $table WHERE _id='".$values['_id']."'";
    $result = mysqli_query( $base, $sql );
    if(mysqli_num_rows($result) == 1){
      // Document exists
      mysqli_query($base,"UPDATE $table SET 
        `name`='".$values['name']."',
        `office_id`='".$values['office_id']."',
        `comment`=".$values['comment'].",
        `lastEditedBy`=".$values['lastEditedBy'].",
        `lastEditedAt`=".$values['lastEditedAt']."
        WHERE _id='".$values['_id']."'"
      );
      echo "Updated successfully";
    }else{
      echo "id does not exists";
    }
    mysqli_close($base);
    break;

  ////////////////////////////
  //        DELETE
  ///////////////////////////
  case "deleteDoc":
      $sql = "SELECT * FROM $table WHERE _id='".$data->_id."'";
      $result = mysqli_query( $base, $sql );
      if(mysqli_num_rows($result) == 1){
        // Document exists
        mysqli_query($base,"DELETE FROM $table WHERE _id='".$data->_id."'");
        echo "Deleted successfully";
      }else{
        echo "Serial Number does not exist.";
      }
      mysqli_close($base);
      break;
  ////////////////////////////
  //        GET DOC BY ID
  ///////////////////////////
  case "docById":
    $sql = 
    "SELECT 
      *
    FROM 
      $table 
    WHERE _id='".$data->_id."'";
    if ($result = mysqli_query( $base, $sql )){
        while ($row = mysqli_fetch_assoc($result)) {
        $response = $row;
        }
        if ($result->num_rows == null) {
          echo "No match!";
        } else {
          echo  json_encode($response);;
        }
    };
    mysqli_close($base);
    break;
    ////////////////////////////
    //        GET DOC BY name
    ///////////////////////////
    case "docByName":
      $sql = 
      "SELECT 
        *
      FROM 
        $table 
      WHERE name='".$data->name."'";
        if ($result = mysqli_query( $base, $sql )){
          while ($row = mysqli_fetch_assoc($result)) {
            $response = $row;
          }
          if ($result->num_rows == null) {
            echo "No match!";
          } else {
            echo  json_encode($response);;
          }
      };
      mysqli_close($base);
      break;
    default: echo "No match!";
    break;
}

?>